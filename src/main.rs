use std::env;

mod models;
mod format;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Vec<String> = env::args().collect();
    println!("arg length is {}",args.len());
    if args.len() < 3 {
        println!("Usage; cargo run <server> <community> <Optional page size>");
        std::process::exit(1);
    }

    let mut page_size = 10;
    let server = &args[1];
    let community = &args[2];

    if args.len() > 3 {
        page_size = args[3]
        .parse::<i32>()
        .expect("parsing failed for 3rd argument");
    }
    
    let mut page = models::build_page(page_size, server.to_string(), community.to_string());
    page.download().await;
    Ok(())
}
