use serde::{Deserialize, Serialize};
use std::fs::OpenOptions;
use super::format;
use std::path::Path;
use std::fs::File;
use std::io::Write;
use std::{thread, time};
use std::fs;


#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Post {
    pub id: i64,
    pub name: String,
    pub url: Option<String>,
    pub body: Option<String>,
    #[serde(rename = "creator_id")]
    pub creator_id: i64,
    #[serde(rename = "community_id")]
    pub community_id: i64,
    pub removed: bool,
    pub locked: bool,
    pub published: String,
    pub updated: Option<chrono::NaiveDateTime>,
    pub deleted: bool,
    pub nsfw: bool,
    pub stickied: bool,
    #[serde(rename = "embed_title")]
    pub embed_title: Option<String>,
    #[serde(rename = "embed_description")]
    pub embed_description: Option<String>,
    #[serde(rename = "embed_html")]
    pub embed_html: Option<String>,
    #[serde(rename = "thumbnail_url")]
    pub thumbnail_url: Option<String>,
    #[serde(rename = "ap_id")]
    pub ap_id: String,
    pub local: bool,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Creator {
    pub id: i64,
    pub name: String,
    #[serde(rename = "display_name")]
    pub display_name: Option<String>,
    pub avatar: Option<String>,
    pub banned: bool,
    pub published: String,
    pub updated: Option<chrono::NaiveDateTime>,
    #[serde(rename = "actor_id")]
    pub actor_id: String,
    pub bio: Option<String>,
    pub local: bool,
    pub banner: Option<String>,
    pub deleted: bool,
    #[serde(rename = "inbox_url")]
    pub inbox_url: String,
    #[serde(rename = "shared_inbox_url")]
    pub shared_inbox_url: String,
    #[serde(rename = "matrix_user_id")]
    pub matrix_user_id: Option<String>,
    pub admin: bool,
    #[serde(rename = "bot_account")]
    pub bot_account: bool,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Community {
    pub id: i64,
    pub name: String,
    pub title: String,
    pub description: String,
    pub removed: bool,
    pub published: String,
    pub updated: Option<chrono::NaiveDateTime>,
    pub deleted: bool,
    pub nsfw: bool,
    #[serde(rename = "actor_id")]
    pub actor_id: String,
    pub local: bool,
    pub icon: Option<String>,
    pub banner: Option<String>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Counts {
    pub id: i64,
    #[serde(rename = "post_id")]
    pub post_id: i64,
    pub comments: i64,
    pub score: i64,
    pub upvotes: i64,
    pub downvotes: i64,
    pub stickied: bool,
    pub published: Option<chrono::NaiveDateTime>,
    #[serde(rename = "newest_comment_time_necro")]
    pub newest_comment_time_necro: Option<chrono::NaiveDateTime>,
    #[serde(rename = "newest_comment_time")]
    pub newest_comment_time: Option<chrono::NaiveDateTime>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PostWrapper {
    pub post: Post,
    pub creator: Creator,
    pub community: Community,
    #[serde(rename = "creator_banned_from_community")]
    pub creator_banned_from_community: bool,
    pub counts: Counts,
    pub subscribed: bool,
    pub saved: bool,
    pub read: bool,
    #[serde(rename = "creator_blocked")]
    pub creator_blocked: bool,
    #[serde(rename = "my_vote")]
    pub my_vote: Option<String>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Root {
    pub posts: Vec<PostWrapper>,
}


pub struct PageBuilder {
    pub page: String,
    pub base_query: String,
    pub community: String,
    pub server: String,
    pub page_size: i32,
    pub dir_name: String,
    pub index_file: String,
    pub client: reqwest::Client,
    pub file: std::fs::File,
    pub page_counter: i32,
    pub counter: i32,
}


pub fn build_page(page_size:i32,server:String,community:String) -> PageBuilder {
    let dir_name = format!("./{}", community);
    let index_file = format!("./{}/index.html", community);
    let _ = fs::create_dir_all(dir_name.clone());
    let _ = fs::create_dir_all(dir_name.clone()+"/assets");
    let file = OpenOptions::new()
        .create(true)
        .write(true)
        .append(true)
        .create(true)
        .open(&index_file)
        .unwrap();

     let page = format!(
        r#"<!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8"/>
    <title>{}</title>
    <style>
    img {{
        max-width: 650px;
    }}
    @import 'https://fonts.googleapis.com/css?family=Open+Sans';

* {{
-webkit-box-sizing: border-box;
box-sizing: border-box;
}}

body {{
font-family: 'Open Sans', sans-serif;
line-height: 1.75em;
font-size: 16px;
background-color: #222;
color: #aaa;
}}

.simple-container {{
max-width: 675px;
margin: 0 auto;
padding-top: 70px;
padding-bottom: 20px;
}}

.simple-print {{
fill: white;
stroke: white;
}}
.simple-print svg {{
height: 100%;
}}

.simple-close {{
color: white;
border-color: white;
}}

.simple-ext-info {{
border-top: 1px solid #aaa;
}}

p {{
font-size: 16px;
}}

h1 {{
font-size: 30px;
line-height: 34px;
}}

h2 {{
font-size: 20px;
line-height: 25px;
}}

h3 {{
font-size: 16px;
line-height: 27px;
padding-top: 15px;
padding-bottom: 15px;
border-bottom: 1px solid #D8D8D8;
border-top: 1px solid #D8D8D8;
}}

hr {{
height: 1px;
background-color: #d8d8d8;
border: none;
width: 100%;
margin: 0px;
}}

a[href] {{
color: #1e8ad6;
}}

a[href]:hover {{
color: #3ba0e6;
}}


li {{
line-height: 1.5em;
}}

aside,
[class *= "sidebar"],
[id *= "sidebar"] {{
max-width: 90%;
margin: 0 auto;
border: 1px solid lightgrey;
padding: 5px 15px;
}}

@media (min-width: 1921px) {{
body {{
    font-size: 18px;
}}
}}

    </style>
    </head>
    <body>

    <h1>{} at {} </h2>

    "#,
        community,community,server
    );

    let base_query = format!(
        "{}/api/v3/post/list?community_name={}&limit={}&page=",
        server, community, page_size
    );

    PageBuilder{community:community,
        server:server,
        file:file,
        page:page,
        base_query:base_query,
        client:reqwest::Client::builder().build().expect("client build failed"),
        page_size:page_size,
        dir_name:dir_name,
        index_file:index_file,
        page_counter:1,
        counter:1,
    }
}

impl PageBuilder {
    async fn handle_url(&mut self,url:String){
        let _ = writeln!(self.file, "<a href={}>{}</a>", &url, &url);
        let _ = writeln!(self.file, "<br><br>");

        let response = reqwest::get(&url).await;

        match response {
            Err(err) => {
                println!("Error geting from url {}",err);
            },
            Ok(res) => {
                let ending = format::return_ending(url.to_string());

                if ending != "".to_string() {
                    if ending == "mp4".to_string() {
                        let video_tag = format!(
                            r#" <video width="600"  controls>
                                <source src="./assets/{}.mp4" type="video/mp4">
                                Your browser does not support the video tag.
                            </video> "#,
                            self.counter
                        );

                        let _ = writeln!(self.file, "{}", video_tag);
                    }
                    else {
                        let _ = writeln!(
                            self.file,
                            "<img class=displayed src=./assets/{}.{} alt={}>",
                            self.counter, ending, self.counter
                        );
                    }
                    let filename = format!("./{}/assets/{}.{}", self.community, self.counter, ending);
                    let path = Path::new(&filename);
                    match File::create(&path) {
                        Err(why) => panic!("couldn't create {}", why),
                        Ok(mut file) => {
                            let content = res.bytes().await;
                            match content {
                                Err(err)=> {
                                    println!("Error {}",err);
                                },
                                Ok(cont) => {
                                    let _ = file.write_all(&cont);
                                }
                            }
                        },
                    };
                    
                    
                }
            }
        }
    }  
}



impl PageBuilder {
    //community,community,server
    pub async fn download(&mut self) {
        let mut retry_count = 0;
        
        let _ = write!(self.file, "{}", &self.page);
        loop {
            if retry_count >2 {
                println!("Retry count exceded. Are your server and community details correct?");
                break;
            }
            let query = format!("{}{}",self.base_query,self.page_counter);
            println!("Query:{}",query);
            let res = self.client.get(query).send().await;

            match res {
                Err(err) => {
                    println!("Error on request {}",err);
                    retry_count = retry_count + 1;
                    continue;
                },
                Ok(response) => {
                    if response.status() != 200 {
                        retry_count = retry_count +1; 
                        println!("rate limit likely hit. sleeping 5 seconds");
                        let five_seconds = time::Duration::from_secs(5);
                        thread::sleep(five_seconds);
                        continue;
                    }
                    self.page_counter = self.page_counter +1 ; 
                    retry_count = 0;
                    let p = response.json::<Root>().await;

                    match p {
                        Err(err) => {
                            println!("Error on json thingy {}",err);

                        },
                        Ok(posts) => {
                            if posts.posts.len() <1{
                                println!("no more data");
                                break;
                            }
                            for post in posts.posts.iter() {
                                println!("Backing up post {}", self.counter);
                                if let Err(e) = writeln!(self.file, "<h2>{}</h2>", &post.post.name) {
                                    eprintln!("Couldn't write to file: {}", e);
                                }
                                let _ = writeln!(self.file, "<p>By: {}</p>", &post.creator.name);
                                match &post.post.body {
                                    Some(body) => {
                                        let _ = writeln!(self.file, "<p>{}</p>", &body);
                                    }
                                    _ => {}
                                }

                                match &post.post.url {
                                    Some(url) => {
                                        self.handle_url(url.to_string()).await;
                                    },
                                    _ =>{},
                                }

                                self.counter = self.counter +1 ;

                            }

                        }
                    }
                }
            }

        }

        let footer = "</body>
        </html>";
        let _ = write!(self.file, "{}", &footer);

    }
       
}